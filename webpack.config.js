const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');

const htmlWebpackPlugin = new HtmlWebPackPlugin({
  template: './public/index.html',
  filename: './index.html',
});

module.exports = {
  entry: './src/index.tsx',
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, 'dist'),
    filename: 'index_bundle.js',
    chunkFilename: "[name].chunk.js"
  },
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve('public')
  },
  module: {
    rules: [
      {
        test: /\.tsx$|\.ts$/,
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      { // node_modules scss
        test: /\.scss$/,
        include: path.resolve(__dirname, "node_modules"),
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      { // project scss
        test: /\.scss$/,
        exclude: path.resolve(__dirname, "node_modules"),
        use: [
          "style-loader",
          "css-loader?modules",
          "sass-loader",
          "@teamsupercell/typings-for-css-modules-loader",
        ],
      },
      {
        test: /\.(png|svg|jpg|gif|pdf|csv|zip|jar)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            context: ''
          },
        },
      },
    ],
  },
  resolve: {
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules')
    ],
    extensions: ['.js', '.jsx', '.tsx', '.ts', '.scss'],
  },
  plugins: [htmlWebpackPlugin],
};
