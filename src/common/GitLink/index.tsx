import Config from "Config";
import * as React from "react";
import styles from "./index.scss";

interface IGitLinkProps {
    uri: string;
}

export default (props: IGitLinkProps) => {
    return (
        <table className={styles.gitlink}>
            <tbody>
                <tr>
                    <td><img src={require("asset/img/gitlab-logo.png")} /></td>
                    <td><a href={Config.gitLink + props.uri}>Megtekintés a git repóban</a></td>
                </tr>
            </tbody>
        </table>
    );
};
