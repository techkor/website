import * as hljs from "highlight.js";
import "highlight.js/styles/default.css";
import * as React from "react";
import styles from "./index.scss";

interface ICodeProps {
  lang?: string;
  inline?: boolean;
  children: React.ReactNode;
}

export default class Code extends React.Component<ICodeProps> {
  private blockRef: React.RefObject<HTMLElement>;

  constructor(props: ICodeProps) {
    super(props);
    this.blockRef = React.createRef();
  }

  public componentDidMount() {
    hljs.highlightBlock(this.blockRef.current);
  }

  public render() {
    const lang = this.props.lang || "plain";
    const { children, inline } = this.props;
    if (inline) {
      return (
        <code className={styles.inline + " " + (lang ? lang : "plain")} ref={this.blockRef}>{children}</code>
      );
    } else {
      return (
        <pre><code className={lang ? lang : "plain"} ref={this.blockRef}>{children}</code></pre>
      );
    }
  }
}
