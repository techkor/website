import { CollectionComponent, ICollectionComponentProps } from "common/Collection";
import Collection from "common/Collection/Collection";
import IItem from "common/Collection/IItem";
import * as React from "react";
import { ReactNode } from "react";
import * as Loadable from "react-loadable";
import { Link, Route, RouteComponentProps, Switch } from "react-router-dom";
import styles from "./index.scss";

export type ITaskKey = string;

export interface ITask extends IItem<ITaskKey> {
  name: string;
  content: Promise<IContentModule>;
}

export interface ITaskMeta {
  title: string;
  baseUri: string;
}

export type ITaskCollection = Collection<ITaskMeta, ITaskKey, ITask>;

interface IUrlParameters {
  taskUri: string;
}

const Loading = () => {
  return <p>Betöltés</p>;
};

export interface IContentModule {
  default: React.ComponentType;
}

const LoadableContent = (promise: Promise<IContentModule>) => {
  return Loadable({
    loader: () => promise,
    loading: Loading,
  });
};

export class TaskCollectionComponent extends CollectionComponent<ITaskMeta, ITaskKey, ITask> {

  constructor(props: ICollectionComponentProps<ITaskMeta, ITaskKey, ITask>) {
    super(props);
    this.meta = this.props.collection.meta;
  }

  public renderList(): React.SFC {
    return () => {
      const listItems: ReactNode[] = this.props.collection.items.map((item) =>
        (<li key={item.key}><Link to={`${this.meta.baseUri}/${item.key}`}>{item.name}</Link></li>));
      return (
        <>
          <h1>{this.meta.title}</h1>
          <ul>{listItems}</ul>
        </>
      );
    };
  }

  public renderItem(key: string): React.SFC {
    return () => {
      const item = this.props.collection.getItem(key);
      return (
        <div className={styles.taskcontent}>
          <h1>{item.name}</h1>
          {React.createElement(LoadableContent(item.content), undefined)}
        </div>
      );
    };
  }

  public extractTaskUri(): React.SFC<RouteComponentProps<IUrlParameters>> {
    return (props) => {
      return this.renderItem(props.match.params.taskUri)({});
    };
  }

  public render() {
    return (
      <Switch>
        <Route exact path={this.meta.baseUri} component={this.renderList()} />
        <Route exact path={`${this.meta.baseUri}/:taskUri`} component={this.extractTaskUri()} />
      </Switch>
    );
  }

}
