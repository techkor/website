export abstract class ItemBase<Key> {
    public abstract getKey(): Key;
}
