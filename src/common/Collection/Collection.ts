import IItem from "./IItem";

export default class Collection<Meta, Key, Item extends IItem<Key>> {
  public items: Item[];
  public meta: Meta;

  public constructor(collection: Partial<Collection<Meta, Key, Item>>) {
    Object.assign(this, collection);
  }

  public getItem(key: Key): Item {
    for (const item of this.items) {
      if (item.key === key) {
        return item;
      }
    }
  }
}
