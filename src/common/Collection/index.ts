import {Component, ReactNode} from "react";
import Collection from "./Collection";
import IItem from "./IItem";

export interface ICollectionComponentProps<Meta, Key, Item extends IItem<Key>> {
  collection: Collection<Meta, Key, Item>;
}

export abstract class CollectionComponent<Meta, Key, Item extends IItem<Key>>
  extends Component<ICollectionComponentProps<Meta, Key, Item>> {

  protected meta: Meta;
  protected items: Map<Key, Item>;

  public abstract renderList(): ReactNode;

  public abstract renderItem(index: Key): ReactNode;

}
