import * as React from "react";
import { Collapse } from "reactstrap";
import styles from "./index.scss";

interface ISolutionProps {
  children: React.ReactNode;
}

interface ISolutionState {
  open: boolean;
}

export default class Solution extends React.Component<ISolutionProps, ISolutionState> {

  constructor(props: ISolutionProps) {
    super(props);
    this.state = {
      open: false,
    };
  }

  public render() {
    return (
      <div className={styles.solution}>
        <span onClick={this.toggle.bind(this)}>Megoldás</span>
        <Collapse isOpen={this.state.open}>
          <div className={styles.content}>
            {this.props.children}
          </div>
        </Collapse>
      </div>
    );
  }

  private toggle() {
    this.setState((state) => ({ open: !state.open }));
  }

}
