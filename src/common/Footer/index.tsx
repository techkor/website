import * as React from "react";
import styles from "./index.scss";

export default function render(): JSX.Element {
  return (
    <>
      <div className={styles["footer-placeholder"]} />
      <div className={styles.footer}><p>Tech kör @ 2019</p></div>
    </>
  );
}
