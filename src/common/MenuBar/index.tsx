import Config from "Config";
import * as React from "react";
import { Link } from "react-router-dom";
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem } from "reactstrap";
import { getClassLabel, lastClass, lastClassUrl } from "../../pages/Classes";
import styles from "./index.scss";

interface IMenuBarState {
  isOpen: boolean;
}

export default class MenuBar extends React.Component<{}, IMenuBarState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  public render() {
    return (
      <Navbar dark expand="lg" className={styles["navbar-custom"]}>
        <NavbarBrand className={styles["navbar-brand"]} href="/">
          <img src={require("asset/img/logo.svg")} width="30" height="30" alt="logo" />
          Tech kör
        </NavbarBrand>
        <NavbarToggler onClick={this.toggle.bind(this)} />

        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav navbar className="ml-auto">
            <NavItem><Link className="nav-link" to="/alkalmak">Alkalmak</Link></NavItem>
            {/*<NavItem><Link className="nav-link" to={lastClassUrl}>{getClassLabel(lastClass)}</Link></NavItem>*/}
            <NavItem><Link className="nav-link" to="/tananyag">Tananyag</Link></NavItem>
            <NavItem><Link className="nav-link" to="/feladatok">Feladatok</Link></NavItem>
            <NavItem><Link className="nav-link" to="/projektek">Projektek</Link></NavItem>
            <NavItem><a className="nav-link" href={Config.gitLink}>Gitlab</a></NavItem>
            <NavItem>
              <a className="nav-link" href={Config.facebookLink}>Facebook</a>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }

  private toggle() {
    this.setState((state) => ({ isOpen: !state.isOpen }));
  }
}
