import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/scss/bootstrap.scss";
import { createBrowserHistory } from "history";
import * as React from "react";
import * as Loadable from "react-loadable";
import { Route, Router, Switch } from "react-router-dom";

import Footer from "./common/Footer";
import MenuBar from "./common/MenuBar";

import "./App.scss";

const history = createBrowserHistory();

const Loading = () => {
  return <p>Betöltés</p>;
};

interface ILoadableModule {
  default: React.ComponentType;
}

const LoadablePage = (loader: () => Promise<ILoadableModule>) => {
  return Loadable({
    loader,
    loading: Loading,
  });
};
const MainPage = LoadablePage(() => import(/* webpackChunkName: "MainPage" */ "pages/MainPage"));
const Classes = LoadablePage(() => import(/* webpackChunkName: "Classes" */ "pages/Classes"));
const Material = LoadablePage(() => import(/* webpackChunkName: "Material" */ "pages/Material"));
const Tasks = LoadablePage(() => import(/* webpackChunkName: "Material" */ "pages/Tasks"));
const Projects = LoadablePage(() => import(/* webpackChunkName: "Tasks" */ "pages/Projects"));
const NotFound = LoadablePage(() => import(/* webpackChunkName: "NotFound" */ "pages/NotFound"));

export default function App() {
  return (
    <Router history={history}>
      <>
        <MenuBar />

        <section>
          <div className="container">
            <Switch>
              <Route exact path="/" component={MainPage} />
              <Route path="/alkalmak" component={Classes} />
              <Route path="/tananyag" component={Material} />
              <Route path="/feladatok" component={Tasks} />
              <Route path="/projektek" component={Projects} />
              <Route component={NotFound} status={404} />
            </Switch>
          </div>
        </section>

        <Footer />
      </>
    </Router>
  );
}
