
import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import unregisterServiceWorker from "./registerServiceWorker";

ReactDOM.render(<App />, document.getElementById("root"));
unregisterServiceWorker();
