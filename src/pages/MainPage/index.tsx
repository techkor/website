import * as React from "react";

export default function render(): JSX.Element {
  return (
    <div>
      <h1>Tech kör</h1>
      <p>
        A Tech kör, illetve az elődje, 2015 és 2019 között működött a Berzsenyi falain belül, mint programozó szakkör.
        Sokféle témakör és feladat előkerült, ezeket a honlapon továbbra is elérhetőként hagyjuk.
      </p>
      <p>Üdvözlettel:</p>
      <a href="mailto:balazs.frey@techkor.hu" className="d-block">Frey Balázs</a>
      <a href="mailto:bence.hornak@techkor.hu" className="d-block">Hornák Bence</a>
    </div>
  );
}
