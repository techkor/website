import GitLink from "common/GitLink";
import * as React from "react";
import { IClass } from "../..";

export default {
  content: (
    <>
      <GitLink uri="/kmeans"/>
    </>
  ),
  num: 22,
  title: "K-közép (gépi tanulás)",
} as IClass;
