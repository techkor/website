import * as React from "react";
import { IClass } from "../..";
import styles from "./index.scss";

export default {
  content: (
    <>
      <p>Hol lehet kijutni a közepéről?:</p>
      <img src={require("./maze.png")} className={styles.maze} />
    </>
  ),
  num: "5_hf",
  overrideClassNumber: "Otthoni alkalom",
  title: "Labirintus",
} as IClass;
