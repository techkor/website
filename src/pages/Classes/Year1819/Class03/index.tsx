import * as React from "react";
import {Link} from "react-router-dom";
import { IClass } from "../..";

export default {
  content: (
    <>
      <h2>Ismétlés</h2>
      <p>Újaknak:</p>
      <ul>
        <li><Link to={"/feladatok/ismerkedes"}>Ismerkedés a Java nyelvvel</Link></li>
      </ul>
      <p>Akik jártak tavaly is:</p>
      <ul>
        <li><Link to="/feladatok/wifi">Wi-Fi</Link></li>
      </ul>

      <h2>Új anyag</h2>
      <p>Újaknak:</p>
      <ul>
        <li><Link to="/feladatok/ismerkedes-a-fuggvenyekkel">Ismerkedés a függvényekkel</Link></li>
        <li><Link to="/feladatok/gyorsetterem">Gyorsétterem</Link></li>
      </ul>

      <p>Akik régóta járnak:</p>
      <ul>
        <li><Link to="/feladatok/dobozok">Dobozok</Link></li>
      </ul>
    </>
  ),
  num: 3,
  title: "Alkossunk valamit!",
} as IClass;
