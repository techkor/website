import * as React from "react";
import { IClass } from "../..";

export default {
    content: (
        <>
            <p>Objektumok gyakorlás, öröklődés</p>
        </>
    ),
    num: 16,
    title: "Öröklődés",
} as IClass;
