import Code from "common/Code";
import * as React from "react";
import { IClass } from "../..";

export default {
  content: (
    <>
      <p>Mindenki kap hozzáférést egy távoli Linux virtuális géphez.</p>
      <h2>Csatlakozás</h2>
      <ol>
        <li>
          A csatlakozáshoz haszználjuk a{" "}
          <a href="https://www.putty.org/">PuTTY</a>-t.
        </li>
        <li>
          Szerezzük meg a csatlakozáshoz szükséges kulcsot (
          <Code inline>*.ppk</Code>)
        </li>
        <li>A kapcsolat típusának válasszuk az SSH-t</li>
        <li>
          A kulcsot állítsuk be a{" "}
          <Code inline>
            Connection > SSH > Auth > Private key for authentication
          </Code>{" "}
          mezőnél
        </li>
        <li>
          Illetve érdemes átállítani a terminál típusát színesre, ehhez
          válasszuk a{" "}
          <Code inline>
            Connection > Data > Terminal details > Terminal-type string:
            xterm-256color
          </Code>{" "}
          opciót
        </li>
      </ol>
      <h2>Hasznos parancsok</h2>
      <ul>
        <li>
          <p>Fájlok kilistázása a working directory-ban:</p>
          <Code lang="sh">ls -l</Code>
        </li>
        <li>
          <p>Mappaváltás</p>
          <Code lang="sh">cd &lt;új mappa&gt;</Code>
        </li>
        <li>
          <p>Fájl szerkesztése</p>
          <Code lang="sh">nano &lt;fájlnév&gt;</Code>
        </li>
        <li>
          <p>Mappa létrehozása</p>
          <Code lang="sh">mkdir &lt;mappanév&gt;</Code>
        </li>
        <li>
          <p>Leírás egy parancshoz:</p>
          <Code lang="sh">man &lt;parancs&gt;</Code>
          <p>
            (pl.{" "}
            <Code inline lang="sh">
              man ls
            </Code>
            ). Kilépés a <Code inline>q</Code>-val.
          </p>
        </li>
        <li>
          <p>Fájlkezelő (Midnight Commander/mc)</p>
          <p>Telepítés:</p>
          <Code lang="sh">sudo apt install mc</Code>
          <p>Utána:</p>
          <Code lang="sh">mc</Code>
        </li>
        <li>
          <p>Gép "pingelése"</p>
          <Code lang="sh">ping &lt;cím&gt;</Code>
        </li>
        <li>
          <p>Távoli géphez vezető útvonal kiírása:</p>
          <Code lang="sh">traceroute &lt;cím&gt;</Code>
        </li>
        <li>
          <p>Hálózati kapcsolat</p>
          <ul>
            <li>
              <p>Hallgatás:</p>
              <Code lang="sh">nc -l &lt;port&gt;</Code>
            </li>
            <li>
              <p>Csatlakozás:</p>
              <Code lang="sh">nc &lt;cím&gt; &lt;port&gt;</Code>
            </li>
          </ul>
        </li>
      </ul>
    </>
  ),
  num: 18,
  title: "Hálózat, linux",
} as IClass;
