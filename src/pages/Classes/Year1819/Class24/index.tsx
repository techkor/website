import * as React from "react";
import { IClass } from "../..";
import GitLink from "../../../../common/GitLink";

export default {
  content: (
    <>
      <h2>Évzárás</h2>
      <p><b>Morgan Stanley záróesemény:</b> június 6. (csütörtök) 16:30-18:00, indulás közösen a
      Berzsenyiből <b>15.40-kor</b>!</p>
      <a href="http://www.morganstanley.com/iweb/surveymatic/webapp/ext/jsp/extindex.jsp?surveyId=97592">Kérdőív</a>

      <h2>Feladat</h2>
      <GitLink uri="/astar" />
    </>
  ),
  num: 24,
  title: "A* algoritmus",
} as IClass;
