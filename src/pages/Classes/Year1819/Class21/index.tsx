import GitLink from "common/GitLink";
import * as React from "react";
import { IClass } from "../..";

export default {
  content: (
    <>
      <GitLink uri="/regression"/>
    </>
  ),
  num: 21,
  title: "Gépi tanulás bevezetés",
} as IClass;
