import * as React from "react";
import { IClass } from "../..";

export default {
  content:
    <>
      <h2>Átalakuló világ</h2>
      <p>Milyen új kihívások jelentek meg az elmúlt 20 évben? Milyen ma létező foglalkozások azok, amikről 20 évvel
        ezelőtt senki nem tudta, hogy egyszer kelleni fog?</p>

      <h2>Mivel foglalkozik egy{"\u2026"}</h2>
      <ol>
        <li>Rendszermérnök</li>
        <li>Etikus hacker</li>
        <li>Frontend fejlesztő</li>
        <li>Backend fejlesztő</li>
        <li>Beágyazott szoftverfejlesztő</li>
        <li>Rendszergazda</li>
      </ol>

      <h2>Az atomoktól az univerzumig</h2>
      <p>A fizikai világhoz hasonlóan az informatikában is nagyon különböző dolgokat látunk, ha különböző méretű
        nagyítót használunk.</p>
      <ol>
        <li>
          <p>A számítástechnikai eszközök szinte kizárólagosan elektronikával működnek, annak előnyös tulajdonságai
            miatt. Minden használatban lévő hardver emiatt erősen kihasználja a fizika elektromágneses jelenségeit.</p>
        </li>
        <li>
          <p>Az elektronikai eszközök logikai áramkörökből épülnek fel, amik logikai kapukat tartalmaznak (amik
            egyenként 2-4 tranzisztort tartalmaznak). Logikai a vezetékek kétfajta állapotban lehetnek, ezt a két
            állapotot tekinthetjük 0-snak és 1-esnek, azaz 1 bit információt tudnak reprezentálni. Bitcsoportoknak meg
            bármilyen értelmet tudunk tulajdonítani, ha akarjuk, akkor úgy tekintünk egy 0-1 sorozatra, mint egy számra,
            egy színre egy szóra vagy egy zeneszámra.</p>
        </li>
        <li>
          <p>Logikai áramkörökből tudunk tervezni összetett hálózatokat, amik általános műveleteket tudnak végrehajtani
            a 0-1 sorozatokon. Például összeadhatunk számokat, kirajzolhatunk egy színt a kijelzőre vagy írhatunk
            biteket az eszköz háttértárára. Az ilyen általános célú logikai áramköröket hívják
            processzornak/CPU-nak.</p>
        </li>
        <li>
          <p>A CPU-kat lehet direkten programozni, viszont tényleg csak nagyon egyszerű utasításokkal tudunk operálni.
            Nagyjából a 50-es-60-as évekig programozták közvetlenül a CPU-kat, azóta csak olyan programokat írnak
            ennyire alacsonyszintű nyelven, amiket nagyon muszáj.</p>
        </li>
        <li>
          <p>A múlt század programozói hamar rájöttek, hogy sokkal gyorsabban tudnának programozni, ha használhatnának
            összetettebb utasításokat. Kis trükkök segítségével megoldható, hogy az összetett utasításokat is megértsék
            a CPU-k. A trükköt a fordítóprogramok bevezetésével oldották meg, amik egy jól olvasható forráskódot
            fordítanak át a processzorok nyelvére.</p>
          <p>Fordítóprogramok segítségével lényegesen nagyobb szoftvereket lehetett írni, ráadásul nem kellett különböző
            nyelvet beszélő hardverekre elejétől végéig újraírni minden programot. Így jelentek meg például az első
            kifinomult operációs rendszerek.</p>
        </li>
        <li>
          <p>A fordítóprogramok megjelenése környékén elindult a modern operációs rendszerek fejlesztése. Az
            oprendszerek folyamatosan futnak az eszközökön, feladatuk a felhasználó programjainak futtatása, illetve a
            hardver kezelése. Az oprendszerek jelenítik meg az ablakokat a képernyőn, ezek határozzák meg, hogy mikor
            melyik program futhat, melyiknek mihez van hozzáférése, illetve beszélik a perifériák (egér, nyomtató, Wifi)
            nyelvét.</p>
        </li>
        <li>
          <p>Az operációs rendszer segítségével futtathatunk rengeteg programot. Ezek közül van olyan, ami ahhoz kell,
            hogy a natív kódra fordított nyelveknél még magasabb szinten tudjunk programozni. Ilyen például, egy
            böngésző a Java Virtual Machine vagy a Python Interpreter. <b>Mi erre a szintre fogunk építeni az év
              során.</b></p>
        </li>
        <li>
          <p>
            A következő szint talán már az, amikor nem egy gépen futtatunk egy szoftvert, hanem amikor gépek hada
            dolgozik együtt. A gépek mindegyikén fut oprendszer, futnak programok, és ezek a hálózaton keresztül
            kommunikálnak egymással. Ennek segítségével soha nem látott számítási kapacitást tudunk elérni. Például egy
            {" "}<a href="https://www.youtube.com/watch?v=zDAYZU4A3w0">adatközpontban</a>
          </p>
        </li>
      </ol>
    </>
  ,
  num: 1,
  title: "Vágjunk bele!",
} as IClass;
