import * as React from "react";
import { ReactNode } from "react";
import { IClass } from "../..";
import Code from "../../../../common/Code";
import GitLink from "../../../../common/GitLink";

interface ISpecificationProps {
    signature: string;
    description: ReactNode;
}

function Specification({ signature, description }: ISpecificationProps) {
    return <li>
        <Code lang="java" inline>{signature}</Code>: {description}
    </li>;
}

export default {
    content: (
        <>
            <h2>Modellezés és objektumok</h2>
            <p>
                Nem volt olyan óra eddig, hogy ne használtunk volna változókat. Egyszerűen tök hasznosak, mivel
                eltárolhatunk bennük számokat vagy szöveget mondjuk, amikkel később műveleteket végezhetünk,
                kiszámolhatunk belőlük mindenfélét, kiírhatjuk stb. A változóknak adhatunk számtípust, szövegtípust,
                boolean-típust (igaz/hamis) esetleg ezek valamelyikéből képzett tömbtípust.
            </p>
            <p>
                Felmerül azonban a kérdés, hogy <i>a fenti típusokkal vajon mindenféle dolgot el tudunk tárolni?</i> Mi
                a helyzet bonyolultabb dolgokkal, például egy <i>film adatlappal</i>, <i>egy személy adataival</i> vagy
                egy <i>bank adatbázissal</i>? Egy film adatlaphoz sokféle információ kapcsolódik (például film neve,
                megjelenés dátuma, stáblista, ezek mind különböző típusú információk), azaz minden létrehozott
                adatlaphoz sok féle változót kell létrehoznunk. Ez az eddig megismertek alpján nehéz feladat.
            </p>
            <p>A jó hír, hogy az <i>objektumok</i> pontosan erre jók!</p>
            <p>Példa:</p>
            <Code lang="java">
                {
                    "class Movie {\n"
                    + "    public String name;\n"
                    + "    public int releaseYear;\n"
                    + "    public String[] cast;\n"
                    + "\n"
                    + "    public void printCast() {\n"
                    + "        for(int i=0; i < cast.length; ++i) {\n"
                    + "            System.out.println(cast[i]);\n"
                    + "        }\n"
                    + "    }\n"
                    + "}\n"
                    + "\n"
                    + "\u2026\n"
                    + "    public static void main(String[] args) {\n"
                    + "        Movie interstellar = new Movie();\n"
                    + "        interstellar.name = \"Interstellar\";\n"
                    + "        interstellar.releaseYear = 2014;\n"
                    + "        interstellar.cast = new String[] { \"Matthew McConaughey\", \"Anne Hathaway\", "
                    + "\"Jessica Chastain\" };\n"
                    + "        System.out.println(interstellar.releaseYear);\n"
                    + "    }\n"
                    + "\u2026"
                }
            </Code>

            <h3>Feladat</h3>
            <p>
                Szeretnénk egy bank számára nyilvántartórendszert kidolgozni. A bank rengeteg számlát (account) kezel,
                ezeket Szeretnénk a nyilvántartórendszerünkben eltárolni. Minden számlának van egy egyenlege, amelynek
                értéke egy forintban kifejezett egész szám. Továbbá minden számla egy személyhez köthető (person). A
                személyeket pedig nevükkel, lakhelyük irányítószámával, illetve személyiszámukkal jellemezzük. Egy
                személyhez akár több számla is tartozhat. A számlák között lehetséges utalni (send), amennyiben a
                terhelendő számlán van elég pénz.
            </p>
            <ol>
                <li>Hozz létre egy Person osztályt, amivel egy személyről eltárolhatjuk a nevét, lakhelyének
                    irányítószámát, illetve személyiszámát (típus?)</li>
                <li>Hozz létre egy Account osztályt, amiben egy számla adatai tárolhatók. Egyenleg, illetve a tulajdonos
                    személy (típus?).</li>
                <li>Hozz létre egy Bank osztályt, amiben sok számlát lehet eltárolni.</li>
                <li>Írj egy main metódust, amiben létrehozol egy bankot néhány számlával, illetve a hozzájuk tartozó
                    tulajdonosokkal. Legyen olyan tulajdonos, akihez több számla tartozik.</li>
                <li>Hozz létre egy metódust (tagfüggvény), ami lehetővé teszi egyik számláról egy másikra való utalást,
                    amennyiben rendelkezésre áll a megfelelő fedezet.</li>
            </ol>

            <h2>Nehéz feladatok</h2>
            <p>A múlt óráról maradt még néhány feladat teszttel, hajrá, hajrá! :)</p>
            <GitLink uri="/math-functions" />
            <ul>
                <Specification
                    signature="BigInteger fastPow(BigInteger base, BigInteger exponent)" description={
                        <>
                            Kiszámítja a base<sup>exponent</sup> hatvány értékét, miközben csupán
                                log<sub>2</sub>(exponent) lépést végez. A bemenet és a kimenet egyaránt{" "}
                            <a href="https://docs.oracle.com/javase/10/docs/api/java/math/BigInteger.html">
                                BigInteger
                                </a>.
                        </>
                    } />
                <Specification
                    signature="String numberToString(double number, int radix)" description={
                        <>
                            A number számot kiírja radix-számrendszerben. A number nem feltétlenül egész szám!
                        </>
                    } />
                <Specification
                    signature="double findZero(DoubleUnaryOperator function)" description={
                        <>
                            Közelítő módszerrel kiszámítja, hogy a paraméterként átadott folytonos, szigorúan monoton
                            növő függvény hol vesz fel nulla értéket. Több megoldás esetén bármelyik jó.
                            Például a <Code lang="java" inline>findZero(x => (x-4)*(x-4)*(x-4))</Code> kifejezés a 4.0.
                            Feltételezhető, hogy a függvény zérushelye a [-1000, +1000) intervallumban van.
                        </>
                    } />
            </ul>
        </>
    ),
    num: 12,
    title: "Objektumok, matematikai függvények",
} as IClass;
