import Code from "common/Code";
import GitLink from "common/GitLink";
import * as React from "react";
import {IClass} from "../..";

export default {
  content: (
    <>
      <p>Múlt órán kipróbáltuk, hogyan lehet egyszerű utasításokat adni a robot motorjainak. Ma ezt fogjuk finomítani,
        illetve kipróbáljuk a szenzorokat.</p>

      <h2>Feladatok</h2>
      <ul>
        <li>
          <p>Nyisd meg a múlt órai projektet! Ha hiányoztál, innen tudod betölteni:</p>
          <GitLink uri="/lego-ev3-motor"/>
        </li>
        <li>
          <p>Nézzük át, mi mit jelent!</p>
          <Code lang="java">
            {"import lejos.hardware.motor.EV3LargeRegulatedMotor;\n" +
            "import lejos.hardware.port.MotorPort;\n" +
            "import lejos.robotics.RegulatedMotor;\n" +
            "\n" +
            "public class Main {\n" +
            "\n" +
            "    public static void main(String[] args) throws InterruptedException {\n" +
            "        RegulatedMotor motorB = new EV3LargeRegulatedMotor(MotorPort.A);\n" +
            "        RegulatedMotor motorC = new EV3LargeRegulatedMotor(MotorPort.B);\n" +
            "\n" +
            "        motorB.forward();\n" +
            "        motorC.forward();\n" +
            "\n" +
            "        Thread.sleep(1000);\n" +
            "\n" +
            "        motorB.stop();\n" +
            "        motorC.stop();\n" +
            "    }\n" +
            "\n" +
            "}\n"}
          </Code>
        </li>
        <li>Írj eljárást, amivel előre megy x cm-t</li>
        <li>Írj eljárást, amivel elfordul x°-t</li>
        <li>A robotot vezéreld végig egy ház alakú pályán</li>
        <li>
          <p>Próbáld ki az ultraszonikus szenzort</p>
          <Code lang="java">
            {"import lejos.hardware.motor.EV3LargeRegulatedMotor;\n" +
            "import lejos.hardware.port.MotorPort;\n" +
            "import lejos.hardware.port.SensorPort;\n" +
            "import lejos.hardware.sensor.EV3UltrasonicSensor;\n" +
            "import lejos.robotics.RegulatedMotor;\n" +
            "import lejos.robotics.SampleProvider;\n" +
            "import lejos.utility.Delay;\n" +
            "\n" +
            "public class Main {\n" +
            "\n" +
            "    public static EV3UltrasonicSensor sensor;\n" +
            "    public static SampleProvider provider;\n" +
            "    public static RegulatedMotor motorB, motorC;\n" +
            "\n" +
            "    public static void init() {\n" +
            "        sensor = new EV3UltrasonicSensor(SensorPort.S4);\n" +
            "        provider = sensor.getDistanceMode();\n" +
            "        motorB = new EV3LargeRegulatedMotor(MotorPort.B);\n" +
            "        motorC = new EV3LargeRegulatedMotor(MotorPort.C);\n" +
            "    }\n" +
            "\n" +
            "    public static void close() {\n" +
            "        sensor.close();\n" +
            "        motorB.close();\n" +
            "        motorC.close();\n" +
            "    }\n" +
            "\n" +
            "    public static float getDistance() {\n" +
            "        float[] sample = new float[provider.sampleSize()];\n" +
            "        provider.fetchSample(sample, 0);\n" +
            "        return sample[0];\n" +
            "    }\n" +
            "\n" +
            "    public static void main(String[] args) {\n" +
            "        init();\n" +
            "        \n" +
            "        motorB.forward();\n" +
            "        motorC.backward();\n" +
            "        \n" +
            "        Delay.msDelay(3000);\n" +
            "        \n" +
            "        motorB.stop();\n" +
            "        motorC.stop();\n" +
            "\n" +
            "        for (int i = 0; i < 100; ++i) {\n" +
            "            System.out.println(getDistance());\n" +
            "            Delay.msDelay(300);\n" +
            "        }\n" +
            "\n" +
            "        close();\n" +
            "    }\n" +
            "\n" +
            "}\n"}
          </Code>
        </li>
        <li>A robot menjen egyenesen és akkor álljon meg, amikor 10cm távolságban van a faltól.</li>
        <li>A robot forduljon szembe a fallal!</li>
        <li>Szemléltesd grafikonon, hogy milyen adatokat mért a szenzor!</li>
      </ul>
    </>
  ),
  num: 7,
  title: "Lego Mindstorms EV3 \u2013 szenzorok",
} as IClass;
