import * as React from "react";
import {Link} from "react-router-dom";
import { IClass } from "../..";

export default {
  content: (
    <>
      <p>Újaknak:</p>
      <ul>
        <li><Link to="/feladatok/gyorsetterem">Gyorsétterem</Link></li>
      </ul>

      <p>Akik régóta járnak:</p>
      <ul>
        <li><Link to="/feladatok/amoba-3d">Amőba 3D</Link></li>
        <li><Link to="/feladatok/vektorgrafikus-kepek">Vektorgrafikus képek</Link></li>
      </ul>
    </>
  ),
  num: 5,
  title: "Kis projektek",
} as IClass;
