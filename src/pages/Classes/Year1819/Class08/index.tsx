import * as React from "react";
import {IClass} from "../..";

export default {
  content: (
    <>
      <p>Tananyag:</p>
      <ul>
        <li>Tömbök</li>
        <li>
          Sorozatok
          <ul>
            <li>Számtani</li>
            <li>Mértani</li>
            <li>Fibonacci</li>
            <li>Saját képzési szabály</li>
          </ul>
        </li>
        <li>
          Rendezés
          <ul>
            <li>Buborék rendezés</li>
          </ul>
        </li>
        <li>Mátrixszorzás, transzponálás</li>
      </ul>
    </>
  ),
  num: 8,
  title: "Tömbök, sorozatok",
} as IClass;
