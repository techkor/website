import Code from "common/Code";
import * as React from "react";
import { IClass } from "../..";

export default {
  content: (
    <>
      <p>Mindenki kap hozzáférést egy távoli Linux virtuális géphez.</p>
      <h2>Csatlakozás</h2>
      <ol>
        <li>
          A csatlakozáshoz haszználjuk a{" "}
          <a href="https://www.putty.org/">PuTTY</a>-t.
        </li>
        <li>
          Szerezzük meg a csatlakozáshoz szükséges kulcsot (
          <Code inline>*.ppk</Code>)
        </li>
        <li>A kapcsolat típusának válasszuk az SSH-t</li>
        <li>
          A kulcsot állítsuk be a{" "}
          <Code inline>
            Connection > SSH > Auth > Private key for authentication
          </Code>{" "}
          mezőnél
        </li>
        <li>
          Illetve érdemes átállítani a terminál típusát színesre, ehhez
          válasszuk a{" "}
          <Code inline>
            Connection > Data > Terminal details > Terminal-type string:
            xterm-256color
          </Code>{" "}
          opciót
        </li>
      </ol>
      <h2>Hasznos parancsok</h2>
      <ul>
        <li>
          <p>Fájlok kilistázása a working directory-ban:</p>
          <Code lang="sh">ls -l</Code>
        </li>
        <li>
          <p>Mappaváltás</p>
          <Code lang="sh">cd &lt;új mappa&gt;</Code>
        </li>
        <li>
          <p>Fájl szerkesztése</p>
          <Code lang="sh">nano &lt;fájlnév&gt;</Code>
        </li>
        <li>
          <p>Mappa létrehozása</p>
          <Code lang="sh">mkdir &lt;mappanév&gt;</Code>
        </li>
        <li>
          <p>Leírás egy parancshoz:</p>
          <Code lang="sh">man &lt;parancs&gt;</Code>
          <p>
            (pl.{" "}
            <Code inline lang="sh">
              man ls
            </Code>
            ). Kilépés a <Code inline>q</Code>-val.
          </p>
        </li>
        <li>
          <p>Fájlkezelő (Midnight Commander/mc)</p>
          <p>Telepítés:</p>
          <Code lang="sh">sudo apt install mc</Code>
          <p>Utána:</p>
          <Code lang="sh">mc</Code>
        </li>
        <li>
          <p>Gép "pingelése"</p>
          <Code lang="sh">ping &lt;cím&gt;</Code>
        </li>
        <li>
          <p>Távoli géphez vezető útvonal kiírása:</p>
          <Code lang="sh">traceroute &lt;cím&gt;</Code>
        </li>
        <li>
          <p>Hálózati kapcsolat</p>
          <ul>
            <li>
              <p>Hallgatás:</p>
              <Code lang="sh">nc -l &lt;port&gt;</Code>
            </li>
            <li>
              <p>Csatlakozás:</p>
              <Code lang="sh">nc &lt;cím&gt; &lt;port&gt;</Code>
            </li>
          </ul>
        </li>
        <li>
          <p>Weboldal lekérése HTTP protokollal:</p>
          <Code lang="sh">
            {"nc vanenet.hu 80\nGET / HTTP/1.1\nHost: vanenet.hu\n\n"}
          </Code>
          <p>(Két enter a végén)</p>
        </li>
        <li>
          <p>Webszerver futtatása:</p>
          <Code lang="sh">
            {"apt install python\nsudo python -m http.server 80"}
          </Code>
          <p>Példa oldal:</p>
          <Code lang="html">
            {"<!doctype html>\n" +
              '<html lang="en">\n' +
              "  <head>\n" +
              "    <!-- Required meta tags -->\n" +
              '    <meta charset="utf-8">\n' +
              '    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">\n' +
              "    <!-- Bootstrap CSS -->\n" +
              '    <link rel="stylesheet"' +
              ' href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"' +
              ' integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"' +
              ' crossorigin="anonymous">\n' +
              "    <title>Hello, world!</title>\n" +
              "  </head>\n" +
              "  <body>\n" +
              "<!-- Default form login -->\n" +
              '<div class="container">\n' +
              '<form class="text-center border border-light p-5" target=".">\n' +
              '    <p class="h4 mb-4">Sign in</p>\n' +
              "    <!-- Email -->\n" +
              '    <input type="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail">\n' +
              "    <!-- Password -->\n" +
              '    <input type="password" id="defaultLoginFormPassword" class="form-control mb-4"' +
              ' placeholder="Password">\n' +
              "    <!-- Sign in button -->\n" +
              '    <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>\n' +
              "</form>\n" +
              "</div>\n" +
              "<!-- Default form login -->\n" +
              "    <!-- Optional JavaScript -->\n" +
              "    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\n" +
              '    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"' +
              ' integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"' +
              ' crossorigin="anonymous"></script>\n' +
              '    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"' +
              ' integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"' +
              ' crossorigin="anonymous"></script>\n' +
              '    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"' +
              ' integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"' +
              ' crossorigin="anonymous"></script>\n' +
              "  </body>\n" +
              "</html>"}
          </Code>
        </li>
      </ul>
    </>
  ),
  num: 19,
  title: "Hálózat, linux",
} as IClass;
