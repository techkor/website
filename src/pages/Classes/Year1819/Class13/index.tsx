import Code from "common/Code";
import GitLink from "common/GitLink";
import * as React from "react";
import { ReactNode } from "react";
import { Link } from "react-router-dom";
import { IClass } from "../..";

interface ISpecificationProps {
    signature: string;
    description: ReactNode;
}

function Specification({ signature, description }: ISpecificationProps) {
    return <li>
        <Code lang="java" inline>{signature}</Code>: {description}
    </li>;
}

export default {
    content: (
        <>
            <Link to="/feladatok/mozi">Mozi</Link>
        </>
    ),
    num: 13,
    title: "Modellezés és objektumok",
} as IClass;
