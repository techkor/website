import Code from "common/Code";
import GitLink from "common/GitLink";
import * as React from "react";
import { Link } from "react-router-dom";
import { IClass } from "../..";

export default {
    content: (
        <>
            <Link to="/feladatok/mozi">Mozi</Link>
        </>
    ),
    num: 14,
    title: "Emailküldés és QR-kód generálás",
} as IClass;
