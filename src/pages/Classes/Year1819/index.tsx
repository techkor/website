import { IYear } from "..";
import Class01 from "./Class01";
import Class02 from "./Class02";
import Class03 from "./Class03";
import Class04 from "./Class04";
import Class05 from "./Class05";
import Class05_hf from "./Class05_hf";
import Class06 from "./Class06";
import Class07 from "./Class07";
import Class08 from "./Class08";
import Class09 from "./Class09";
import Class10 from "./Class10";
import Class11 from "./Class11";
import Class12 from "./Class12";
import Class13 from "./Class13";
import Class14 from "./Class14";
import Class15 from "./Class15";
import Class16 from "./Class16";
import Class17 from "./Class17";
import Class18 from "./Class18";
import Class19 from "./Class19";
import Class20 from "./Class20";
import Class21 from "./Class21";
import Class22 from "./Class22";
import Class23 from "./Class23";
import Class24 from "./Class24";

export default {
  baseUri: "/alkalmak",
  classes: [
    Class01,
    Class02,
    Class03,
    Class04,
    Class05,
    Class05_hf,
    Class06,
    Class07,
    Class08,
    Class09,
    Class10,
    Class11,
    Class12,
    Class13,
    Class14,
    Class15,
    Class16,
    Class17,
    Class18,
    Class19,
    Class20,
    Class21,
    Class22,
    Class23,
    Class24,
  ],
  name: "2018-19-es tanév",
} as IYear;
