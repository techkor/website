import * as React from "react";
import { Link } from "react-router-dom";
import { IClass } from "../..";

export default {
  content: (
    <>
      <p>Újaknak:</p>
      <ul>
        <li><Link to={"/feladatok/ismerkedes"}>Ismerkedés a Java nyelvvel</Link></li>
        <li><Link to={"/feladatok/gyorsetterem"}>Gyorsétterem</Link></li>
      </ul>
      <p>Akik jártak tavaly is:</p>
      <ul>
        <li><Link to="/feladatok/wifi">Wi-Fi</Link></li>
        <li><Link to="/feladatok/8-kiralyno"> 8 királynő probléma</Link></li>
      </ul>
    </>
  ),
  num: 2,
  title: "Alapozás, ismétlés",
} as IClass;
