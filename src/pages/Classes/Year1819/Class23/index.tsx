import * as React from "react";
import { IClass } from "../..";

export default {
  content: (
    <>
      <ul>
        <li><a href="http://scs.ryerson.ca/~aharley/vis/conv/">http://scs.ryerson.ca/~aharley/vis/conv/</a></li>
        <li><a href="https://playground.tensorflow.org">https://playground.tensorflow.org</a></li>
      </ul>
    </>
  ),
  num: 23,
  title: "Neurális hálók (gépi tanulás)",
} as IClass;
