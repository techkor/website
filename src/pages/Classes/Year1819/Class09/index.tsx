import * as React from "react";
import {IClass} from "../..";
import Code from "../../../../common/Code";

export default {
  content: (
    <>
      <p>
        A neten rengeteg ingyenesen elérhető adatforrás található mindenféle témakörben (pl. időjárás, forgalom,
        horoszkóp, nap vicce, ...). A mai órán a forint-euró árfolyam változásaival fogunk számolni, ehhez a <a
        href="https://blog.quandl.com/api-for-currency-data">Quandl</a> API-ját használjuk. <i>(API: Application
        Programming Interface, azaz egy felület, melyen keresztül más programok könnyen tudnak interakcióba lépni egy
        adott rendszerrel.)</i>
      </p>
      <p>
        Mint minden API-hoz, tartozik hozzá egy <i>dokumentáció</i>, ami leírja, hogy hogyan lehet adatokat kinyerni a
        rendszerükből. A Quandl API-ját HTTP protokollon keresztül érhetjük el. Részletesen: <a
        href="https://docs.quandl.com/docs/in-depth-usage">dokumentáció</a>
      </p>
      <p>
        A dokumentáció alapján összeszerkeszthetünk egy URL-t, amiben CSV formátumban tudjuk kinyerni a forint-euró
        árfolyam korábbi értékeit. A <a href="https://blog.quandl.com/api-for-currency-data">táblázat</a> alapján van
        egy <Code inline>Euro Exchange Rates</Code> nevű, <Code inline>ECB</Code> azonosítójú adatbázis, amiben van egy
        {" "} <Code inline>EURHUF</Code> nevű tábla, ez kell nekünk. Ezek alapján az URL:
      </p>
      <p>
        <a href="http://www.quandl.com/api/v3/datasets/ECB/EURHUF/data.csv">
          http://www.quandl.com/api/v3/datasets/ECB/EURHUF/data.csv
        </a>
      </p>
      <p>Nyissuk meg a CSV fájlt szövegszerkesztővel, táblázatkezelővel! Készítsünk grafikont az adatokból!</p>
      <p>
        Írjunk programot, ami beolvassa a standard inputon érkező CSV fájlt, és a következő műveleteket hajtja végre:
      </p>
      <ol>
        <li>
          A bemenetet elmenti egy tömbbe (segítség: <a href="https://stackoverflow.com/a/14274319">StackOverflow</a>)
          <Code lang="java">
            {"Scanner scanner = new Scanner(new File(\"data.csv\"));\n" +
            "scanner.useDelimiter(\",\");\n" +
            "scanner.useLocale(Locale.ENGLISH);\n" +
            "while(scanner.hasNext()){\n" +
            "    System.out.print(scanner.next()+\"|\");\n" +
            "}\n" +
            "scanner.close();"}
          </Code>
        </li>
        <li>Kiírja a maximum értéket</li>
        <li>Kiírja a maximum időpontját</li>
        <li>
          Kiírja, hogy mely időtartományokban volt az euró értéke 300 forint felett (pl. <Code inline>2017-01-03 -
          2017-02-01</Code>, csak egy példa a formátumra)
        </li>
      </ol>

      <p>Nehezebb feladatok:</p>
      <ol>
        <li>A program töltse le az élő adatokat a szerverről. Javasolt megoldás: <a
          href="https://square.github.io/okhttp/">okhttp</a> segítségével.
        </li>
        <li>
          Vizualizáció: a program jelenítse meg egy charton az értékeket. Ingyenes library:{" "}
          <a href="https://github.com/knowm/XChart">XChart</a>
        </li>
        <li>
          Rajzold fel a pontok konvex burkát!
        </li>
      </ol>
    </>
  ),
  num: 9,
  title: "Tömbök, valutaárfolyam",
} as IClass;
