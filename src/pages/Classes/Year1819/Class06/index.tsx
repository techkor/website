import Code from "common/Code";
import GitLink from "common/GitLink";
import * as React from "react";
import {IClass} from "../..";

export default {
  content: (
    <>
      <p>A mai órán a LEGO Mindstorms EV3 robotra fogunk programozni Java nyelven.</p>
      <img src={require("./mindstorms.jpg")}
           style={{width: "100%", maxWidth: "700px", margin: "0 auto", display: "block"}}/>

      <p>A robotot nem az alapértelmezett operációs rendszerével használjuk, hanem a <a
        href="http://www.lejos.org/">leJOS</a> rendszert, mivel ezáltal tudunk Java kódot futtatni az eszközön.</p>

      <p>A standard beépített elemeken túl most nyilván további függvényeket szeretnénk használni, amik lehetővé teszik
        a robot motorjainak mozgatását, a szenzorok értékeinek olvasását. Ezért a NetBeans-es projektünkhöz hozzá kell
        adni <a href={require("./lib.zip")}>néhány szoftverkönyvtárat</a> (ebben majd segítünk).</p>

      <h2>Feladat</h2>
      <p>Próbáld ki a motorokat! Indítsd el, állítsd meg, menjen gyorsan, lassan, előre, hátra!</p>
      <p>Segítség:</p>
      <Code lang="java">
        {"import lejos.hardware.motor.EV3LargeRegulatedMotor;\n" +
        "import lejos.hardware.port.MotorPort;\n" +
        "import lejos.robotics.RegulatedMotor;\n" +
        "\n" +
        "public class Main {\n" +
        "\n" +
        "    public static void main(String[] args) throws InterruptedException {\n" +
        "        RegulatedMotor motorA = new EV3LargeRegulatedMotor(MotorPort.A);\n" +
        "        RegulatedMotor motorB = new EV3LargeRegulatedMotor(MotorPort.B);\n" +
        "\n" +
        "        motorA.forward();\n" +
        "        motorB.forward();\n" +
        "\n" +
        "        Thread.sleep(1000);\n" +
        "\n" +
        "        motorA.stop();\n" +
        "        motorB.stop();\n" +
        "    }\n" +
        "\n" +
        "}\n"}
      </Code>

      <h2>A teljes projekt</h2>
      <GitLink uri="/lego-ev3-motor" />
    </>
  ),
  num: 6,
  title: "Lego Mindstorms EV3 \u2013 motorok",
} as IClass;
