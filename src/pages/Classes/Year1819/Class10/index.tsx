import * as React from "react";
import {IClass} from "../..";
import Code from "../../../../common/Code";

export default {
  content: (
    <>
      <p>
        Ha az interneten szenzitív adatokat küldünk, amiket nem szeretnénk, ha akárki megszerezhetne, felmerül az igény
        arra, hogy a kommunikáció során alkalmazzunk titkosítást. Viszont ez egyáltalán nem egy triviális feladat,
        nézzünk meg pár kihívást!
      </p>
      <p>
        Az egyszerűség kedvéért feltételezzük, hogy Alice szeretné elküldeni a telefonja PIN kódját Bobnak.
      </p>

      <h2>One time padding</h2>
      <p>
        Kódoló függvény: <Code inline>kódol(üzenet) = kulcs XOR üzenet</Code>, ahol a XOR művelete a bitenkénti kizáró
        vagy.
      </p>
      <h3>Kérdések</h3>
      <ol>
        <li>Hogyan kell dekódolni?</li>
        <li>Milyen kulcsra van szükség?</li>
        <li>
          Csak a kódolt üzenet ismeretében lehet-e következtetést levonni a kulccsal vagy az üzenettel kapcsolatban?
        </li>
        <li>
          Ha ismerjük az üzenetet és a kódolt üzenetet, ki lehet-e következtetést levonni a kulccsal kapcsolatban?
        </li>
        <li>Lehet-e többször felhasználni a kulcsot?</li>
        <li>Hogyan kell átjuttatni a kulcsot Alice-től Bob-nak?</li>
      </ol>

      <h2>Kulcscsere protokoll</h2>
      <p><Code inline>h(x)</Code> legyen egy hash függvény, ami x-ből csinál "valamit". Feltétel, hogy ne legyen könnyű
        kitalálni, hogy hányszor alkalmaztuk egymás után egy adott számon (vagyis <Code inline>h(h(...h(x)...))</Code>
        -ben pontosan hány h van).</p>
      <h3>Algoritmus</h3>
      <h4>Alice</h4>
      <ol>
        <li><Code inline>a := random 0 és N-1 között</Code></li>
        <li>
          <Code inline>A := h(h(...h(1)...))</Code>, azaz számolja ki h(1)-et, majd ezt beadva a h függvénynek, ... ezt
          pontosan <Code inline>a</Code>-szor
        </li>
        <li>Elküldi <Code inline>A</Code>-t Bobnak</li>
        <li>Megkapja <Code inline>B</Code>-t Bobtól</li>
        <li>
          <Code inline>K := h(h(...h(B)...))</Code>, ahol megint <Code inline>a</Code>-szor hajtja végre a h függvényt.
        </li>
      </ol>
      <h4>Bob</h4>
      <ol>
        <li><Code inline>b := random 0 és N-1 között</Code></li>
        <li>
          <Code inline>B := h(h(...h(1)...))</Code>, azaz számolja ki h(1)-et, majd ezt beadva a h függvénynek, ... ezt
          pontosan <Code inline>b</Code>-szer
        </li>
        <li>Elküldi <Code inline>B</Code>-t Alice-nek</li>
        <li>Megkapja <Code inline>A</Code>-t Alice-től</li>
        <li>
          <Code inline>K := h(h(...h(A)...))</Code>, ahol <Code inline>b</Code>-szer hajtja végre a h függvényt.
        </li>
      </ol>
      <h4>Eredmény</h4>
      <p>Mindkettejük birtokában meg lesz a <Code inline>K</Code> szám, anélkül, hogy az átment volna a csatornán.</p>
      <h3>Kérdések</h3>
      <ol>
        <li>Egy támadó a csatornát figyelve miért nem tudná előállítani A-ból és B-ből K-t?</li>
        <li>Hogyan lehetne <Code inline>log(a)</Code> időben kiszámítani <Code inline>h(h(...h(1)...))</Code>-et
          (összesen <Code inline>a</Code> darab)?
        </li>
        <li>Hogyan lehet összekombinálni a One Time Padding-gel?</li>
        <li>Passzív támadó (aki csak figyel) ki tudja-e találni K-t?</li>
        <li>Aktív támadó (aki megváltoztathat üzeneteket) ki tudja-e játszani a protokollt?</li>
      </ol>
    </>
  ),
  num: 10,
  title: "Titkosítás",
} as IClass;
