import * as React from "react";
import { ReactNode } from "react";
import { IClass } from "../..";
import Code from "../../../../common/Code";
import GitLink from "../../../../common/GitLink";

interface ISpecificationProps {
    signature: string;
    description: ReactNode;
}

function Specification({ signature, description }: ISpecificationProps) {
    return <li>
        <Code lang="java" inline>{signature}</Code>: {description}
    </li>;
}

export default {
    content: (
        <>
            <p>Mindenek előtt egy{" "}
                <a href="https://kahoot.it">gyors Kahoot</a>.</p>
            <h2>Feladat</h2>
            <p>Hozzátok létre a függvényeket a leírások alapján! Rendelkezésre állnak tesztek, amik helyes
                implementáció esetén sikeresen lefutnak.</p>
            <p>A kiinduló projekt:</p>
            <GitLink uri="/math-functions" />

            <h3>Könnyebbek</h3>
            <ul>
                <Specification signature="int cube(int x)" description={<>Kiszámolja x<sup>3</sup>-t</>} />
                <Specification signature="int pow(int x, int y)" description={
                    <>Kiszámolja x<sup>y</sup>-t. Feltételezhető, hogy y >= 0.</>
                } />
                <Specification signature="long factorial(int n)" description={<>Kiszámolja az n!-t.</>} />
                <Specification signature="double vectorLength(double x, double y)"
                    description={<>Megadja az (x, y) vektor hosszát.</>} />
                <Specification signature="double distance(double x1, double y1, double x2, double y2)"
                    description={<>Megadja az (x1, y1) és (x2, y2) pontok eklideszi (szokásos) távolságát.</>} />
                <Specification signature="double manhattanDistance(double x1, double y1, double x2, double y2)"
                    description={<>Kiszámítja  az (x1, y1) és (x2, y2) pontok{" "}
                        <a href="https://en.wikipedia.org/wiki/Taxicab_geometry">Manhattan-távolságát</a>.</>} />
                <Specification signature="boolean insideCircle(double r, double x, double y)"
                    description={<>Igazzal tér vissza, ha az (x, y) pont az origó körüli r sugarú kör{" "}
                        <i>belsejében</i> található, egyébként hamissal.</>} />
            </ul>

            <h3>Nehezebbek</h3>
            <ul>
                <Specification
                    signature={"double[] solveEquations(double a11, double a12, double b1,"
                        + " double a21, double a22, double b2)"}
                    description={
                        <>Megold egy kétismeretlenes egyenletrendszert, ahol az egyenletek alakja:
                    <Code>{"a11\u00b7x1+a12\u00b7x2=b1\n"
                                + "a21\u00b7x+a22\u00b7x2=b2"}
                            </Code>
                            A visszatérési érték egy kételemű <Code inline lang="java">double</Code> tömb x1 és x2
                            értékével, ha nincs megoldás, akkor egy üres tömb.
                </>
                    } />
                <Specification
                    signature="BigInteger fastPow(BigInteger base, BigInteger exponent)" description={
                        <>
                            Kiszámítja a base<sup>exponent</sup> hatvány értékét, miközben csupán
                                log<sub>2</sub>(exponent) lépést végez. A bemenet és a kimenet egyaránt{" "}
                            <a href="https://docs.oracle.com/javase/10/docs/api/java/math/BigInteger.html">
                                BigInteger
                                </a>.
                        </>
                    } />
                <Specification
                    signature="String numberToString(double number, int radix)" description={
                        <>
                            A number számot kiírja radix-számrendszerben. A number nem feltétlenül egész szám!
                        </>
                    } />
                <Specification
                    signature="double findZero(DoubleUnaryOperator function)" description={
                        <>
                            Közelítő módszerrel kiszámítja, hogy a paraméterként átadott folytonos, szigorúan monoton
                            növő függvény hol vesz fel nulla értéket. Több megoldás esetén bármelyik jó.
                            Például a <Code lang="java" inline>findZero(x => (x-4)*(x-4)*(x-4))</Code> kifejezés a 4.0.
                            Feltételezhető, hogy a függvény zérushelye a [-1000, +1000) intervallumban van.
                        </>
                    } />
            </ul>
        </>
    ),
    num: 11,
    title: "Matematikai függvények",
} as IClass;
