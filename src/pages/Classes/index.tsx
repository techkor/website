import * as React from "react";
import {ReactNode} from "react";

import Year1819 from "./Year1819";
import YearComponent from "./YearComponent";

export interface IClass {
  num?: number | string;
  title: string;
  content: ReactNode;
  overrideClassNumber?: string;
}

export interface IYear {
  baseUri: string;
  name: string;
  classes: IClass[];
}

export function getClassLabel(clazz: IClass) {
  const {num, overrideClassNumber} = clazz;
  return overrideClassNumber || `${num}. alkalom`;
}

const lastYear = Year1819;
export const lastClass = lastYear.classes[lastYear.classes.length - 1];
export const lastClassUrl = "/alkalmak/" + lastClass.num;

export default function render() {
  return (
    <YearComponent year={Year1819}/>
  );
}
