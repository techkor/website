import * as React from "react";
import {Component} from "react";
import {Link, Route, RouteComponentProps, Switch} from "react-router-dom";
import {getClassLabel, IClass, IYear} from "..";

interface IYearComponentProps {
  year: IYear;
}

interface IMatchProps {
  number?: string;
}

export default class YearComponent extends Component<IYearComponentProps> {

  private static getClassNumber(props: RouteComponentProps<IMatchProps>): string {
    return props.match.params.number;
  }

  private classes: {[key: string]: IClass};

  public constructor(props: IYearComponentProps) {
    super(props);
    this.classes = {};
    for (const clazz of this.props.year.classes) {
      this.classes[String(clazz.num)] = clazz;
    }
  }

  public renderClass(num: string) {
    const clazz = this.classes[num];
    return (
      <>
        <h1>
          {getClassLabel(clazz) + ": " + clazz.title}
        </h1>
        {clazz.content}
      </>
    );
  }

  public renderList() {
    const {year} = this.props;
    const {name, classes} = year;
    return (
      <>
        <h1>Alkalmak</h1>
        <h2>{name}</h2>
        <ul>
          {classes.map((clazz, index) => (
            <li key={index}>
              {getClassLabel(clazz)}
              {": "}
              <Link to={`/alkalmak/${clazz.num}`}>{clazz.title}</Link>
            </li>
          ))}
        </ul>
      </>
    );
  }

  public render() {
    const {year} = this.props;
    return (
      <Switch>
        <Route exact path={year.baseUri} component={() => this.renderList()}/>
        <Route exact path={year.baseUri + "/:number"} component={
          (props: RouteComponentProps<IMatchProps>) => this.renderClass(YearComponent.getClassNumber(props))
        }/>
      </Switch>
    );
  }
}
