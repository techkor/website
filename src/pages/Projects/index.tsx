import { ITaskCollection, TaskCollectionComponent } from "common/TaskCollection";
import * as React from "react";
import Collection from "../../common/Collection/Collection";

const projects: ITaskCollection = new Collection({
  items: [
    {
      content: import("./chatszoba"),
        key: "chatszoba",
      name: "Chat szoba",
    },
    {
      content: import("./ko-papir-ollo"),
        key: "ko-papir-ollo",
      name: "Kő-papír-olló",
    },
  ],
  meta: {
    baseUri: "/projektek",
    title: "Projektek",
  },
});

const Projects: React.SFC<{}> = () => {
  return (
    <>
      <TaskCollectionComponent collection={projects} />
      {/*<img src={require("../../asset/img/mikorleszszunet/screenshot.png")} />*/}
    </>
  );
};
export default Projects;
