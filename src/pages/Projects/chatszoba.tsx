import GitLink from "common/GitLink";
import * as React from "react";

export default () => (
    <>
        <h2>Bevezetés</h2>
        <p>
            Ahhoz, hogy kicsit jobban megérthessük, hogyan működnek a hálózati alkalmazások, vegyünk egy példát! Írjunk
            egy alkalmazást, amellyel egy hálózaton belül kommunikálni lehet. A felhasználók egy "szobába" kerülnek,
            ahol a küldött üzeneteket mindannyian megkapják. Küldhetnek az üzenetek mellett <i>ablakrezgetést</i>, ami a
            többi felhasználó csetablakát megrázza.
        </p>

        <h2>A kommunikáció iránya</h2>
        <img src={require("asset/img/chatszoba/server-clients.png")} />

        <h2>Protokoll</h2>
        <p>Ahhoz, hogy több számítógép együtt tudjon működni, egy nyelvet kell beszélniük. A közös nyelvet szokás{" "}
            <i>protokollnak</i> nevezni. A használt nyelv pontos leírása itt található:</p>
        <GitLink uri="/chatroom-server/blob/master/README.md" />
        <p>
            A kliensek két kapcsolatot építenek fel a szerverrel. Az egyiket (<i>'action connection'</i>) arra
            használják, hogy közöljék a szerverrel, ha a felhasználó kiváltott egy eseményt (pl. üzenet küldése vagy
            ablakrezgetés). A másikon (<i>'event connection'</i>) pedig folyamatosan fülel minden kliens, ezen várják a
            szerver üzeneteit arról, hogy egy másik felhasználó milyen üzenetet küldött.
        </p>

        <h2>Az elkészült program</h2>
        <h3>A program forráskódja</h3>
        <h4>Kliens</h4>
        <p><i>A kommentezésért hálás köszönet Szalay Bálintnak!</i></p>
        <GitLink uri="/chatroom-client" />
        <h4>Szerver</h4>
        <GitLink uri="/chatroom-server" />

        <h3>Screenshot</h3>
        <img src={require("asset/img/chatszoba/screenshot.png")}/>
    </>
);
