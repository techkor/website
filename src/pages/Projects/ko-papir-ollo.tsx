import Code from "common/Code";
import GitLink from "common/GitLink";
import * as React from "react";

export default () => (
  <>
    <h2>Bevezetés</h2>
    <p>
      A Kő-papír-ollóról sokan úgy tartják, hogy egy olyan játék, amiben az eredmény csakis a szerencsétől függ. Mégis
      világszerte szerveznek bajnokságokat, amiken a résztvevő "profik" simán elverik a kezdő kő-papír-olló
      játékosokat. Ez márpedig arra enged következtetni, hogy ebben a játékban is van jobb stratégia a puszta
      véletlennél...
    </p>

    <p>
      Járjunk utána a kérdésnek, készítsünk magunknál jobb kő-papír-olló játékost! Ismerjen ki minket, tudja
      megtippelni, hogy mit mutatunk a következő körben!
    </p>

    <h2>A megoldás módja</h2>
    <p>
      Az emberek random-generátora egy idő után kiismerhető. Egy idő után észrevehető, hogy bizonyos minták sokkal
      gyakrabban bukkannak fel, mint mások. Egy kő-papír-olló játékos program pont ezeket a mintákat keresi, és ezeket
      fordítja az emberi ellenfele ellen. Írjunk egy programot, ami egy belső adatszerkezetben eltárolja, hogy mely{" "}
      <i>n</i> hosszúságú kombináció után hányszor következett kő, hányszor papír, hányszor olló. Valahogy így (n=2):
    </p>
    <Code lang="plain">
      {
        `kő-kő:       kő 1, papír 3, olló 0
kő-papír:    kő 0, papír 1, olló 4
kő-olló:     kő 2, papír 1, olló 6
papír-kő:    kő 1, papír 4, olló 2
papír-papír: kő 2, papír 4, olló 0`}
    </Code>
    <p>
      Ha a játékos legutolsó mutatásai sorban ezek voltak: <code>... - kő - papír - papír - kő - olló</code>, akkor
      érdemes arra tippelni, hogy ollót fog rakni, mivel a <code>kő - olló</code> páros után általában azt tett. Tehát
      ilyenkor érdemes a következő körben az ollót megverő követ rakni.
    </p>

    <h2>Az elkészült megoldás</h2>
    <h3>Az elkészült program forráskódja</h3>
    <GitLink uri="/ko-papir-ollo" />
    <h3>Screenshot</h3>
    <img src={require("asset/img/ko-papir-ollo/screenshot.png")} />
    <h3>Leírás</h3>
    <p>A program a nyílbillentyákkel irányítható, <code>balra: kő, fel: papír, jobbra: olló</code>.</p>
  </>
);
