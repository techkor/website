import { ITaskCollection, TaskCollectionComponent } from "common/TaskCollection";
import * as React from "react";
import Collection from "../../common/Collection/Collection";

const tasks: ITaskCollection = new Collection({
  items: [
    {
      content: import("./tasks/8-kiralyno"),
      key: "8-kiralyno",
      name: "8 királynő probléma",
    },
    {
      content: import("./tasks/amoba-3d"),
      key: "amoba-3d",
      name: "Amőba 3D",
    },
    {
      content: import("./tasks/dobozok"),
      key: "dobozok",
      name: "Dobozok",
    },
    {
      content: import("./tasks/gyorsetterem"),
      key: "gyorsetterem",
      name: "Gyorsétterem",
    },
    {
      content: import("./tasks/ismerkedes"),
      key: "ismerkedes",
      name: "Ismerkedés a Java nyelvvel",
    },
    {
      content: import("./tasks/ismerkedes-a-fuggvenyekkel"),
      key: "ismerkedes-a-fuggvenyekkel",
      name: "Ismerkedés a függvényekkel",
    },
    {
      content: import("./tasks/mozi"),
      key: "mozi",
      name: "Mozi",
    },
    {
      content: import("./tasks/vektorgrafikus-kepek/index"),
      key: "vektorgrafikus-kepek",
      name: "Vektorgrafikus képek",
    },
    {
      content: import("./tasks/wifi"),
      key: "wifi",
      name: "Wi-Fi",
    },
  ],
  meta: {
    baseUri: "/feladatok",
    title: "Feladatok",
  },
});

const Tasks: React.SFC<{}> = () => {
  return (
    <TaskCollectionComponent collection={tasks} />
  );
};
export default Tasks;
