import Code from "common/Code";
import * as React from "react";
import {HashLink} from "react-router-hash-link";

export default () => (
  <>
    <p>Sok gyorsétteremben használnak számítógépes rendszert a rendelések kezelésére, mert egyszerűen gyorsabb a
      kiszolgálás, illetve a vendégek is könnyen észreveszik, ha elkészült a rendelésük.</p>

    <p>Nézzük is meg, hogyan működik:</p>
    <ol>
      <li>
        A vendég odalép a kasszához, leadja a rendelését. A kasszás beviszi egy gépbe, ekkor a vendég kap egy sorszámot,
        amit figyelnie kell egy nagy képernyőn, mert ha megjelenik, elkészült a rendelése.
      </li>
      <li>
        A bevitt rendelés megjelenik egy konyhán dolgozó alkalmazott képernyőjén, aki azonnal el tudja kezdeni az étel
        készítését, összeállítását.
      </li>
      <li>
        Miután a konyhán elkészült az étel, az alkalmazott jelzi ezt a rendszernek, ami egyből küld egy értesítést a
        pult felett kihelyezett képernyőre, azaz megjelenik nagy betűkkel az elkészült rendelés sorszáma.
      </li>
      <li>
        A vendég így odalép, és átveszi a rendelését.
      </li>
    </ol>

    <p>A feladatunk az lesz, hogy létrehozzunk egy hasonló elven működő programot.</p>

    <h2>Megvalósítás módja</h2>
    <p>A bevitelt a konzol segítségével valósítsuk meg, mind a rendelés leadását, mind a rendelés elkészülését. A
      különböző kimeneteket különböző fájlokba írjuk. Például, ha a <b>konzolra</b> azt írja egy felhasználó, hogy</p>
    <Code>
      {"rendel Csirkés szendvics\n" +
      "rendel Gulyásleves\n" +
      "kész 1\n"}
    </Code>
    <p>azzal lead két rendelést a kasszás alkalmazott, illetve jelzi, hogy az 1-es sorszámú elkészült.</p>
    <p>A <b>konzolon</b> mindegyikre érkezik egy visszaigazoló üzenet:</p>
    <Code>
      {"Rendelés feldolgozva. Sorszám: 1\n" +
      "Rendelés feldolgozva. Sorszám: 2\n" +
      "A rendelés átvehető 1 sorszámmal\n"}
    </Code>

    <p>A <b>konyha.txt</b> fájlban a rendelések jelennek meg:</p>
    <Code>
      {"Rendelés érkezett: Csirkés szendvics\n" +
      "Rendelés érkezett: Gulyásleves\n"}
    </Code>

    <p>A <b>kivetito.txt</b> fájlban pedig csak az elkészült ételek:</p>
    <Code>
      {"A rendelés átvehető 1 sorszámmal\n"}
    </Code>

    <h2>Megoldás</h2>
    <p><HashLink to="/feladatok/ismerkedes#elso-sor-beolvasasa">Az első sor beolvasása</HashLink> részt átdolgozva
      eljutottunk odáig, hogy egy végtelen ciklusban kiírtuk megerősítésként a rendelést:</p>

    <Code lang="java">
      {"package hu.techkor.gyorsetterem;\n" +
      "\n" +
      "import java.util.Scanner;\n" +
      "\n" +
      "public class Gyorsetterem {\n" +
      "\n" +
      "    /**\n" +
      "     * Mindig a main függvényben indul a program végrehajtása.\n" +
      "     */\n" +
      "    public static void main(String[] args) {\n" +
      "\n" +
      "        // A Scanner segít nekünk beolvasni. Később nézzük részletesen, hogy mit\n" +
      "        // jelent pontosan a következő sor.\n" +
      "        Scanner scanner = new Scanner(System.in);\n" +
      "\n" +
      "        // A while ciklus egészen addig ismételgeti a benne lévő kódot, amíg a\n" +
      "        // kerek zárójelbe írt feltétele igaz. Ha fixen true-t írunk bele, az\n" +
      "        // azt jelenti, hogy örökké ismételgetni fogja a tartalmát.\n" +
      "        while (true) {\n" +
      "            // Létrehozunk egy 'rendelés' nevű változót, aminek az értéke a\n" +
      "            // konzolról beolvasott sor lesz\n" +
      "            String rendelés = scanner.nextLine();\n" +
      "            // Írjuk ki megerősítésként a rendelést feldolgozva\n" +
      "            System.out.println(\"Rendelés érkezett: \" + rendelés);\n" +
      "        }\n" +
      "    }\n" +
      "}\n"}
    </Code>
  </>
);
