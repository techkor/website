import Code from "common/Code";
import * as React from "react";

export default () => (
  <>
    <p>Írjunk néhány matematikai függvényt! A függvények úgy működnek, mint a masinák az általános iskolás
      tankönyvekben. Fent bemegy néhány szám (vagy valami más) és alul kipottyan a végeredmény.</p>
    <p>Valósítsuk meg az abszolútérték függvényt, ami egy egész számból kiszámítja annak abszolútértékét, illetve a max
      függvényt, ami a két bemenete közül a nagyobbikkal tér vissza.</p>
    <Code lang="java">
      {"public class Main {\n" +
      "\n" +
      "    /**\n" +
      "     * Abszolútérték függvény. A 'masina' neve `abs`, egy egész szám típusú\n" +
      "     * bemenetet vár, amit x-szel nevezünk (ezt jelenti az `int x` kifejezés). A\n" +
      "     * 'masinából' egy egész szám potyog ki (erre utal a függvény neve előtti\n" +
      "     * `int` szócska), aminek a kiszámítását a { és } karakterek között írunk\n" +
      "     * le. A `public static` jelentésével egyelőre nem foglalkozunk, minden\n" +
      "     * függvényünknél használni fogjuk őket.\n" +
      "     *\n" +
      "     * @param x Az x paraméter a függvény bemenete\n" +
      "     * @return A függvény az x abszolút értékével tér vissza.\n" +
      "     */\n" +
      "    public static int abs(int x) {\n" +
      "        if (x >= 0) { // Ha x nagyobb vagy egyenlő 0,\n" +
      "            return x; // akkor x-szel térünk vissza\n" +
      "        } else { // Különben\n" +
      "            return -x; // x (-1)-szeresével\n" +
      "        }\n" +
      "    }\n" +
      "\n" +
      "    /**\n" +
      "     * Két paraméter közül a nagyobbal tér vissza. A függvénynek két bemenete\n" +
      "     * van, `a` és `b`, melyek egyaránt egész számok (erre utal az `int a, int\n" +
      "     * b` rész). A függvény egy egész számot ad vissza (a függvénynév előtti int\n" +
      "     * erre utal), mégpedig a két szám közül a nagyobbat.\n" +
      "     *\n" +
      "     * @param a Az egyik egész szám bemenet.\n" +
      "     * @param b A másik egész szám bemenet.\n" +
      "     * @return A függvény a nagyobbal tér vissza.\n" +
      "     */\n" +
      "    public static int max(int a, int b) {\n" +
      "        if (a > b) { // Ha a nagyobb mint b,\n" +
      "            return a; // Akkor a-val térünk vissza\n" +
      "        } else { // Különben\n" +
      "            return b; // b-vel\n" +
      "        }\n" +
      "    }\n" +
      "\n" +
      "    /**\n" +
      "     * A main függvény fog elsőként elindulni, itt kell leírnunk, hogy milyen\n" +
      "     * függvényeket szeretnénk meghívni.\n" +
      "     */\n" +
      "    public static void main(String[] args) {\n" +
      "        // Így tudunk kiírni egy fix számot:\n" +
      "        System.out.println(13); // Kiírja, hogy 13\n" +
      "\n" +
      "        // Hívjuk meg az abs függényt -12-re, írjuk ki az eredményt\n" +
      "        System.out.println(abs(-12)); // Kiírja a -12 abszolútértékét, azaz 12-t\n" +
      "        System.out.println(abs(130)); // Kiírja a 130 abszolútértékét, a 130-at\n" +
      "\n" +
      "        // Hívjuk meg a max függvényt, írjuk ki egyből az eredményt\n" +
      "        System.out.println(max(2, 7)); // Kiírja a 2 és a 7 közül a nagyobbat, a 7-et\n" +
      "        System.out.println(max(-1, -10)); // -1\n" +
      "\n" +
      "        System.out.println(\"Vége\"); // Kiírja, hogy 'Vége'\n" +
      "    }\n" +
      "\n" +
      "}\n"}
    </Code>
  </>
);
