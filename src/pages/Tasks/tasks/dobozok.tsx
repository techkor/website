import Code from "common/Code";
import * as React from "react";

export default () => (
  <>
    <p>
      Egy üzemben kocka alakú dobozokat gyártanak. Egy doboz akkor tehető bele egy másik dobozba, ha a mérete (a kocka
      oldalhossza) kisebb. A dobozokat egy teherautóra pakolhatják, de egymás mellé két doboz nem fér el, azaz csak
      egymásba rakva vihetők el. A dobozok egyesével érkeznek, és a teherautóra csak érkezési sorrendben pakolhatók,
      azaz minden doboznál azt dönthetjük el, hogy beletesszük a teherautón levő dobozokból a legbelső belsejébe vagy
      nem tesszük fel a teherautóra. Írj programot, amely kiírja minden doboz után, hogy ha az az utolsó, amit
      beválogatunk, hány dobozt tudunk vinni maximum, illetve mi a sorszáma annak a doboznak, amibe közvetlenül
      beletettük (0 ha nincs ilyen)
    </p>
    <h2>Bemenet</h2>
    <p>A bemenet két sorból áll, az első sorban N áll, a dobozok száma, a második sorban N darab szám, a megfelelő doboz
      mérete.</p>
    <h2>Kimenet</h2>
    <p>A kimenet N sort tartalmaz, az i-edik sorban A<sub>i</sub> és B<sub>i</sub> áll. A<sub>i</sub> a dobozok
      maximális száma, ha az utolsó dobozt az i-ediknek választjuk, B<sub>i</sub> az a doboz, amibe ilyen esetben bele
      tettük az i-edik dobozt. Ha nincs ilyen, mert az a külső, akkor A<sub>i</sub> értéke 0.</p>
    <h2>Példa</h2>
    <h3>Bemenet</h3>
    <Code>
      {"10\n5 3 6 4 8 7 3 3 1 2"}
    </Code>

    <h3>Kimenet</h3>
    <Code>
      {"1 0\n2 1\n1 0\n2 1\n1 0\n2 5\n3 4\n3 4\n4 7\n4 7\n"}
    </Code>

    <h2>Forrás</h2>
    <p><i>Nemes Tihamér 2017-2018. 1. forduló, 9-10. osztály, 6. feladat</i></p>
  </>
);
