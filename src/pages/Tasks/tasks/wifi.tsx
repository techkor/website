import * as React from "react";

export default () => (
  <>
    <a href={require("./wifi.pdf")}>Feladat</a>
    <p><i>Forrás: <a href={"https://mester.inf.elte.hu"}>Mester</a> > Haladó > Mohó algoritmusok > WiFi</i></p>
  </>
);
