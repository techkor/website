import Code from "common/Code";
import GitLink from "common/GitLink";
import * as React from "react";

export default () => (
    <>
        <p>
            Egy kis mozi kérte a segítségünket, szeretnének bevezetni egy internetes jegyvásárló portált, mely
            segítségével a vásárlóik online tudnak jegyet váltani a vetítéseikre. Szeretnék, ha létrehoznánk egy
            prototípust, amin ki tudják próbálni a jegyvásárlás menetét.
        </p>
        <h2>Részletek</h2>
        <p>
            A moziban termek vannak, amelyekben n×m szék van rendezetten, ahol n a sorok száma, m a soronkénti székek
            száma (minden sorban ugyanannyi szék van, termenként különbözhet n és m).
        </p>
        <p>
            Minden vetítés egy teremhez rendelhető. A vetítést ezen kívül jellemzi az időpontja, illetve a vetített
            film.
        </p>
        <p>
            A filmeknek pedig szeretnénk eltárolni a címét, illetve a megjelenésük évét.
        </p>
        <p>
            Szeretnénk továbbá eltárolni minden eladott jegyet. A jegyeket a vetítésekhez kapcsoljuk, illetve elmentjük
            az email címet, amire a jegyeket rendelték. Sikeres rendelés esetén a rendszer küldjön visszaigazolást az
            email címre.
        </p>
        <p>
            A jegyárakkal és a fizetéssel nem kell foglalkoznunk.
        </p>
        <p>
            A program figyeljen rá, hogy maximum egy jegyet adjon el vetítésenként és székenként.
        </p>
        <h2>Opcionális</h2>
        <p>A program csatoljon a visszaigazoló emailhez egy QR kódot, ami alapján beazonosítható a rendelés.</p>

        <h2>Kiinduló projekt</h2>
        <p>
            A kiinduló projekttel megmutatja, hogyan lehet emailt küldeni a{" "}
            <a href="http://www.simplejavamail.org/">Simple Java Mail</a> segítségével
        </p>
        <GitLink uri="/mozi" />

        <h2>Email küldés</h2>

        <p>
            A kiinduló projekt a <a href="http://www.simplejavamail.org/">Simple Java Mail</a> programkönyvtárat
            használja. Ha egyéb funkciókra lenne szükségünk, használjuk a hivatalos weboldalon található dokumentációt.
        </p>

        <h2>QR-kód</h2>
        <p>
            QR kód létrehozásához használjuk a{" "}
            <a href="https://github.com/zxing/zxing">ZXing ("Zebra Crossing")</a> könyvtárat. Ahhoz, hogy használni
            tudjuk a programkönyvtár osztályait, jeleznünk kell a projektben, hogy a projektünk <i>függősége</i> a{" "}
            <Code inline>zxing</Code> könyvtár (ugyanúgy, mint a <Code inline>Java Simple Mail</Code>, csak most nekünk
            kell ezt megtennünk). Keressük meg a projekt gyökerében lévő <Code inline>pom.xml</Code> fájlt (Files
            fülön). Az XML fájl <Code inline>{"<dependencies>"}</Code> blokkjába szúrjuk be a következő sorokat:
        </p>
        <Code lang="xml">
            {"<dependency>\n" +
                "  <groupId>com.google.zxing</groupId>\n" +
                "  <artifactId>core</artifactId>\n" +
                "  <version>3.3.3</version>\n" +
                "</dependency>\n" +
                "<dependency>\n" +
                "  <groupId>com.google.zxing</groupId>\n" +
                "  <artifactId>javase</artifactId>\n" +
                "  <version>3.3.3</version>\n" +
                "</dependency>"}
        </Code>
        <p>
            Ezután a következő buildnél a NetBeans automatikusan letölti a <Code inline>zxing</Code>-ot.
        </p>
        <p>
            A QR kódot levélhez szeretnénk csatolni, nem pedig fájlba írni, ezért egy bájt tömböt hozunk csak létre
            belőle. Használjuk fel{" "}
            <a href="https://www.callicoder.com/generate-qr-code-in-java-using-zxing/">ennek az oldalnak</a> az egyik
            példáját, ami pont ezt teszi:
        </p>
        <Code lang="java">
            {
                "private static byte[] getQRCodeImage(String text, int width, int height) throws WriterException, " +
                "IOException {\n" +
                "    QRCodeWriter qrCodeWriter = new QRCodeWriter();\n" +
                "    BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);\n" +
                "    \n" +
                "    ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();\n" +
                "    MatrixToImageWriter.writeToStream(bitMatrix, \"PNG\", pngOutputStream);\n" +
                "    byte[] pngData = pngOutputStream.toByteArray(); \n" +
                "    return pngData;\n" +
                "}"
            }
        </Code>
    </>
);
