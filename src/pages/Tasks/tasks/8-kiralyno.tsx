import * as React from "react";
import Code from "../../../common/Code";

export default () => (
  <>
    <p>Maga a feladat összefoglalható egy mondatban, de megoldani talán nem olyan egyszerű. Hogyan (és hányféle módon)
      helyezhető el 8 királynő a sakktáblán úgy, hogy semelyik kettő ne üsse egymást?</p>

    <img src={require("./8-kiralyno_sakk.png")} />

    <h2>Feladat</h2>
    <p>Írjunk programot, ami felsorolja az összes lehetséges megoldást, és megadja azt is, hogy hány van.</p>

    <h2>Bemenet</h2>
    <p>A tábla mérete: 2 {"<= N <= 16"}.</p>

    <h2>Kimenet</h2>
    <p>Az első sorba a megoldások M számát kell írni, majd a következő M sorba egy-egy megoldást. A megoldások N
      szóközzel elválasztott egésszel kódolhatók, ahol az i. szám azt írja le, hogy az i. oszlop hányadik sorában áll a
      királynő. A sorokat és oszlopokat egytől számozzuk.</p>

    <h2>Példa</h2>
    <table>
      <tbody>
        <tr>
          <th>Bemenet</th>
          <th>Kimenet</th>
        </tr>
        <tr>
          <td>
            <Code>4</Code>
          </td>
          <td>
            <Code>
              {"2\n" +
                "2 4 1 3\n" +
                "3 1 4 2"}
            </Code>
          </td>
        </tr>
      </tbody>
    </table>

    <p>
      <i>
        Forrás:{" "}
        <a href="http://info.berzsenyi.hu/programozas/feladatok/8-kiralyno-problema">info.berzsenyi.hu</a>
      </i>
    </p>
  </>
);
