import Code from "common/Code";
import * as React from "react";

export default () => (
  <>
    <p>A feladat egy nyílt forráskódú 3D-s amőba játék lefuttatása, illetve módosítása. Az interneten számtalan nyílt
      forráskódú projekt megtalálható, ezek közül választottunk egyet: <a
        href="https://github.com/nickstanish/TicTacToe3D">https://github.com/nickstanish/TicTacToe3D</a></p>

    <p>Az oldalt fellátogatva lehet online felületen böngészni a projekt kódját, de a legjobb, ha letöltjük az egészet (
      <Code inline>Clone or download > Download ZIP</Code>). Tömörítsük ki a ZIP-et, Netbeans-ben nyissuk meg:</p>
    <ol>
      <li>New Project > Java > Java Project with Existing Sources</li>
      <li>Project Name: TicTacToe3D, Project folder: <Code inline>{"<"}kitömörítés helye{">"}</Code></li>
      <li>(A felugró ablak leokézható)</li>
      <li>Source package Folders: Add Folder: <Code inline>{"<"}kitömörítés helye{">"}/src</Code></li>
    </ol>
    <p>Ha minden sikerült, lefuttatható a projekt.</p>

    <img src={require("./amoba-3d.png")}/>

    <h2>Feladat</h2>
    <p>A feladatok megoldásához nem szükséges a teljes kód megértése, a program úgy van megírva, hogy elég egy kis
      részéhez hozzányúlni ahhoz, hogy egy kis módosítást végrehajtsunk rajt. Használjátok nyugodtan a Netbeans
      beépített funkcióit (pl keresés projektekben)</p>
    <ol>
      <li>A címsorban ne a <Code inline>TicTacToe</Code> szerepeljen, hanem hogy <Code inline>Amőba 3D</Code></li>
      <li>Fordítsátok le a programot magyarra</li>
      <li>Változtassátok meg a kék mezők színét!</li>
      <li>Változtassátok meg a jelölők színét (akár alakját)</li>
      <li>
        Keressétek meg a kódban, hogy mi hívja meg az <Code inline>AI</Code> osztály <Code
        inline>getMove()</Code> metódusát! Használjátok a NetBeans Find Usages opcióját!
      </li>
      <li>Módosítsátok úgy a kódot, hogy az AI 'gondolkodjon' egy másodpercet, mielőtt rak!</li>
      <li>
        Az <Code inline>AI</Code> osztályban az <Code inline>ai</Code> mező egy tömb, ami a legjobb, a második legjobb
        és a harmadik legjobbnak ítélt lépést fogja össze. Módosítsátok úgy a <Code inline>getMove</Code> metódust, hogy
        a könnyű játékos erősebb legyen, azaz 30% eséllyel a legjobb megoldást, 50%-kal a második legjobb megoldást
        adja!
      </li>
      <li>A kész projektet küldjétek el nekünk :)</li>
    </ol>
  </>
)
;
