import Code from "common/Code";
import * as React from "react";

export default () => (
  <>
    <h2>Az első sor kiírása</h2>
    <p>Nyissuk meg a <i>Netbeans</i>-t, aminek segítségével a félév során a programjainkat fogjuk fejleszteni Java
      nyelven. Induljunk az elejétől, hozzunk létre egy egyszerű programot, ami annyit csinál, hogy kiírja a konzolra,
      {" "}<Code inline>Hello world!</Code>. Ehhez hozzunk létre egy új Java projektet. Az alkalmazás neve legyen{" "}
      <Code inline>Gyorsétterem</Code>, a projekt mentési helyét válasszuk ízlés szerint, illetve pipáljuk be a{" "}
      <Code inline>Create main class</Code> opciót, írjuk mellé a következő szöveget:{" "}
      <Code inline>hu.techkor.gyorsetterem.Main</Code>. A <Code inline>Finish</Code> gombra kattintva elkészül a
      projekt, megjelenik az első programunk forráskódja.</p>

    <Code lang="java">
      {"/*\n" +
      " * To change this license header, choose License Headers in Project Properties.\n" +
      " * To change this template file, choose Tools | Templates\n" +
      " * and open the template in the editor.\n" +
      " */\n" +
      "package hu.techkor.ismerkedes;\n" +
      "\n" +
      "/**\n" +
      " *\n" +
      " * @author bence\n" +
      " */\n" +
      "public class Main {\n" +
      "\n" +
      "    /**\n" +
      "     * @param args the command line arguments\n" +
      "     */\n" +
      "    public static void main(String[] args) {\n" +
      "        // TODO code application logic here\n" +
      "    }\n" +
      "    "}
    </Code>

    <p>A szürke részeket kommentnek hívják, nincs semmilyen hatása a program működésére, egyszerűen a kód
      olvashatóságát, értelmezését segítheti. Ha valamit szeretnénk megmagyarázni, érdemes használni. Most igazából
      nincs rá szükségünk, ezért nyugodtan kitörölhetjük az összes kommentet. Ha ezt a kódot <i>lefuttatjuk</i>, nem
      történik egyelőre semmi. Írjunk bele plusz egy sort, ami kiadja a számítógép számára az utasítást, hogy kiírjon
      valamit.</p>

    <Code lang="java">
      {"public class Main {\n" +
      "\n" +
      "    public static void main(String[] args) {\n" +
      "        System.out.println(\"Hello world!\");\n" +
      "    }\n" +
      "    \n" +
      "}\n"}
    </Code>

    <p>A fenti kódot lefuttatva megjelenik a <i>konzolon</i> a <Code inline>Hello world!</Code> felirat. Természetesen a
      kiírt szöveg átírható, érdemes is kipróbálni</p>

    <h2 id="elso-sor-beolvasasa">Az első sor beolvasása</h2>
    <p>Nem túl interaktív az a program, aminek csak kimenete van, bemenete nincs. Tekintsük ezért a következő kódot:</p>

    <Code lang="java">
      {"import java.util.Scanner;\n" +
      "\n" +
      "public class Main {\n" +
      "\n" +
      "    public static void main(String[] args) {\n" +
      "        Scanner scanner = new Scanner(System.in);\n" +
      "        String name = scanner.nextLine();\n" +
      "        System.out.println(\"Hello \"+name+\"!\");\n" +
      "    }\n" +
      "    \n" +
      "}\n"}
    </Code>
    <p>Az első utasítás (<Code lang="java" inline>{"Scanner scanner = new Scanner(System.in);"}</Code>) létrehoz
      egy <Code inline>scanner</Code> nevű <i>objektumot</i>, aminek segítségével a következő sorban
      (<Code lang="java" inline>{"String name = scanner.nextLine();"}</Code>) be tudunk olvasni egy sort a konzolról,
      illetve ezt elmentjük egy <Code inline>name</Code> nevű <i>változóban</i>. Kiírásnál nem egy fix szöveget írunk
      ki, mint a <Code inline>Hello world!</Code> esetén, hanem három részből szerkesztjük össze
      (<Code lang="java" inline>{"\"Hello \"+name+\"!\""}</Code>). A fenti kódot lefuttatva írjuk be a konzolra a
      nevünket, a program üdvözölni fog.</p>
    <Code>
      {"Jani\n" +
      "Hello Jani!"}
    </Code>

    <h2>Rész szöveg kiírása</h2>
    <p>Írjuk ki a beírt névnek csak az első karakterét! Ehhez a <Code lang="java" inline>substring()</Code> eljárást
      fogjuk haszálni:</p>
    <Code lang="java">
      {"import java.util.Scanner;\n" +
      "\n" +
      "public class Main {\n" +
      "\n" +
      "    public static void main(String[] args) {\n" +
      "        Scanner scanner = new Scanner(System.in);\n" +
      "        String name = scanner.nextLine();\n" +
      "        String partial = name.substring(0, 1);\n" +
      "        System.out.println(\"Hello \"+partial+\"!\");\n" +
      "    }\n" +
      "    \n" +
      "}\n"}
    </Code>
    <p>Most egy új változót hoztunk létre, <Code inline>partial</Code> névvel. Figyeljük meg a hasonlóságot és a
      különbséget a <Code inline>name</Code> és a <Code inline>partial</Code> változók definiálásánál.</p>
    <p>Próbáljuk ki, hogy mi történik, ha változtatjuk a <Code lang="java" inline>substring()</Code> eljárás két{" "}
      <i>paraméterét</i>!</p>

    <h2>Szöveg eleje</h2>
    <p>A <Code lang="java" inline>startsWith()</Code> eljárást nagyon hasonlít a{" "}
      <Code lang="java" inline>substring()</Code> eljáráshoz abban, ahogyan őket használni kell. A különbség, hogy más
      {" "}<i>paramétereket</i> kell használni, és mást <i>ad vissza</i>.</p>

    <Code lang="java">
      {"import java.util.Scanner;\n" +
      "\n" +
      "public class Main {\n" +
      "\n" +
      "    public static void main(String[] args) {\n" +
      "        Scanner scanner = new Scanner(System.in);\n" +
      "        String name = scanner.nextLine();\n" +
      "        boolean starts = name.startsWith(\"Ja\");\n" +
      "        System.out.println(starts);\n" +
      "    }\n" +
      "    \n" +
      "}\n"}
    </Code>
  </>
);
