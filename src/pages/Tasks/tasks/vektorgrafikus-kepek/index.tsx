import Code from "common/Code/index";
import Solution from "common/Solution";
import * as React from "react";
import Mate from "./mate";

export default () => (
  <>
    <p>A vektorgrafikus képek egészen más módon írnak le képeket, mint a pixelgrafikusok, amik előkerültek korábban. A
      feladat erejéig nézzük meg, hogyan lehet SVG formátumban leírni képeket.</p>

    <h2>Bolygók</h2>
    <p>Készíts programot, amely egy naprendszer bolygóit vizualizálja. A bemeneten átadjuk a bolygók koordinátáit,
      sugarát és színeit. Ez alapján generálja le a program az SVG képet, amit megnyitva láthatjuk az eredményt.</p>
    <h3>Példa</h3>
    <Code>
      {"50 50 40 red\n" +
      "150 100 30 blue"}
    </Code>
    <svg height="150" width="200">
      <circle cx="50" cy="50" r="40" fill="red"/>
      <circle cx="150" cy="100" r="30" fill="blue"/>
    </svg>
    <Solution>
      <Mate/>
    </Solution>

    <h2>Összekötögetős játék</h2>
    <p>Biztosan megvan az a játék, amikor pöttyöket kell összekötni a számok sorrendjében, és akkor kijön belőle egy
      ábra. Ennek mintájára írj egy programot, ami egy pontsorozatot összeköt, és az eredményt egy SVG kép formájában
      adja ki.</p>
    <h3>Bemenet</h3>
    <a href={require("./pontsorozat.csv")}>pontsorozat.csv</a>
  </>
);
