import Code from "common/Code";
import * as React from "react";

export default function Mate() {
  return (
    <>
      <h3>Kovács Máté megoldása</h3>
      <Code lang={"java"}>
        {"package vectorgafix;\n" +
        "\n" +
        "import java.util.Scanner;\n" +
        "import java.io.*;\n" +
        "\n" +
        "public class Vectorgafix {\n" +
        "\n" +
        "    /**\n" +
        "     * @param args the command line arguments\n" +
        "     */\n" +
        "    public static void main(String[] args) {\n" +
        "       Scanner be = new Scanner(System.in); //beolvasó Scanner létrehozása\n" +
        "       int maxy=0, maxx=0, db=be.nextInt(), w=0, h=0;\n" +
        "       String[] circ = new String[db];\n" +
        "       for(int i=0; i<db; i++) {\n" +
        "           int x=be.nextInt(), y=be.nextInt(),r=be.nextInt();\n" +
        "           circ[i] = \"   <circle cx=\\\"\"+x+\"\\\" cy=\\\"\"+y+\"\\\" r=\\\"\"+r+\n" +
        "                   \"\\\" stroke=\\\"none\\\" fill=\\\"\"+be.nextLine()+\"\\\" />\";\n" +
        "           if(maxx<x) {maxx=x; w=x+r;}\n" +
        "           if(maxy<y) {maxy=y; h=y+r;}\n" +
        "       }\n" +
        "       try{ //file létrehozás, PrintStream létrehozás, kiírás\n" +
        "           PrintStream ki = new PrintStream(new File(\"output.svg\"), \"UTF-8\");\n" +
        "           ki.println(\"<svg xmlns=\\\"http://www.w3.org/2000/svg\\\" " +
        "height=\\\"\"+h+\"\\\" width=\\\"\"+w+\"\\\">\");\n" +
        "           for(int i=0; i<db; i++) {\n" +
        "               ki.println(circ[i]);\n" +
        "           }\n" +
        "           ki.println(\"</svg>\");\n" +
        "           ki.close();\n" +
        "           System.out.println(\"SVG generated successfully. Saved to class folder.\");\n" +
        "       } catch(IOException e) { //hiba kezelése: kiírás err-re\n" +
        "           System.err.println(\"Unable to create SVG. Check file permissions!\");\n" +
        "           System.out.println(\"Permission error; unable to create SVG!\");\n" +
        "           e.printStackTrace();\n" +
        "       }\n" +
        "    }\n" +
        "    \n" +
        "}"}
      </Code>
    </>
  );
}
